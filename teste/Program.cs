﻿using System;
using System.Data.Odbc;
using System.Configuration;
using System.Data;

namespace tmp
{
    class Program
    {
        static void Main(string[] args)
        {
	        OdbcConnection cn;
	        OdbcCommand cmd;
	        string MinhaString;

	        MinhaString="select Now()";
	    
	        /* Caso tenha o odbc.ini
                cn = new OdbcConnection("dsn=nome_dsn_no_odbc_ini");
                cn = new OdbcConnection("dsn=nome_dsn_no_odbc_ini;PWS=senha;UID=usuario");
            */

            /* Caso tenha o odbcinst.ini configurado com o driver
                cn = new OdbcConnection("Driver=SQL Anywhere 16;PWD=senha;UID=usuario;ServerName=nome_banco;CommLinks=tcpip(host=ip_do_servidor;port=porta_do_banco)");
            */
            cn = new OdbcConnection(
                "Driver=/opt/sqlanywhere16/lib64/libdbodbc16.so;" + 
                "PWD=senha;UID=usuario;ServerName=nome_do_banco;" + 
                "CommLinks=tcpip(host=ip_do_servidor;port=porta_do_banco)");
	        cmd = new OdbcCommand(MinhaString,cn);    

	        try
            {
	            cn.Open();
	            Console.WriteLine("Conectado!");
		        using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                Console.WriteLine(reader[i]);
                            }
                        }
                    }
	        }	
            catch (Exception ex)
            {
              	Console.WriteLine(ex.Message);
            }
	        cn.Close();
        }
    }
}
