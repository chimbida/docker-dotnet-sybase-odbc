
## Exemplo de container Docker rodando aplicação dotnet core conectando em base Sql Anywhere via ODBC

  
**A conexão pode ser feita de 2 formas:**

Pelo arquivo odbc.ini, para gerar o arquivo .odbc.ini, basta descomentar e editar as linhas abaixo no arquivo Dockerfile:

```Dockerfile
RUN /opt/sqlanywhere16/bin64/dbdsn -w "NomeDNS" -c "uid=usuario;pwd=senha;eng=nome_base;links=tcpip(host=ip_do_banco;port=porta_do_banco)"
COPY odbcinst.ini /etc/odbcinst.ini
ENV ODBCINI=/root/.odbc.ini
 ```

Ou colocando a string de conexão diretamente na aplicação:

```c#
cn  =  new  OdbcConnection(
	"Driver=/opt/sqlanywhere16/lib64/libdbodbc16.so;"  +
	"PWD=senha;UID=usuario;ServerName=nome_do_banco;"  +
	"CommLinks=tcpip(host=ip_do_servidor;port=porta_do_banco)");
```

Para testar o projeto, basta fazer as alterações na String de conexão e executar:
```bash
git clone https://gitlab.com/chimbida/docker-dotnet-sybase-odbc
cd docker-dotnet-sybase-odbc
docker build -t nome_da_tag_do_container .
docker run -it nome_da_tag_do_container bash
# Dentro do Container
root@1dfad0112391:/app# ./teste
```